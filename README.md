## Problem

A terminal command does not work. `which <command>` shows a correct install, but the command returns **`command not found`.**

Or a correctly quoted or escaped path returns **`file does not exist`**.

# Solution
Maybe the culprit is a [**non-breaking space**](https://en.wikipedia.org/wiki/Non-breaking_space).

**Non-breaking spaces are not whitespace**. The shell interprets them as part of adjacent words. But they **are rendered like spaces**, so you can't see the difference.

You may have *typed* a non-breaking space inadvertedly. Just left a finger a little too long on the `alt` (Mac users: `option`) key, or hacked it down there a little too early — and modified the intended `space`. Or you *copy-pasted* it from somewhere without knowing.

**Re-using commands** with copy-pasting, or from the shell's history (e.g. via `up-arrow` key) **re-uses the non-breaking space** (a source of endless frustration...).

In iTerm2 you can force this mean character out of its camouflage, via an *instant trigger*. Here's how:

## HowTo

1. Go to menu `iTerm2`, select item `Preferences`, then tab `Profiles`. 

1. Select `the profile you want to edit`, then in the right pane the tab `Advanced`, and under `Triggers` click `Edit`. 

1. Then click `➕`  at left bottom to add a new trigger.

1. Set the new profile like in the image. You are free to change the colors, but make sure to have *"instant" checked* and *only one* non-breaking space typed in:. ![](nbsp1.png)

1. Close Triggers Dialog. Done.

## Success

Now, if you type or copy in a non-breaking space, or if a return contains one, iTerm will instantly format it according to your settings.

An example with the above settings:

![](nbsp2.png)

Non-breaking spaces will stand out and you can correct the typo:

![](nbsp4.png)

## Caveat

iTerm2 needs a little time to react to your typing. If you fire a command too fast, the formatting is skipped. 

Compare with the example above:

![](nbsp3.png)

---

P.S.: [**How to type a non-breaking space**](https://en.wikipedia.org/wiki/Non-breaking_space#Keyboard_entry_methods) if you really want to.


